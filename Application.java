public class Application {
	public static void main(String[] args) {
		Student[] students = new Student[3];
		
		students[0] = new Student("Amy", "123456", "Computer Science");
		students[1] = new Student("Nan", "789101", "Computer Science");
		students[2] = new Student("Bob", "112233", "Computer Science");
		
		Classroom classroom = new Classroom("Computers", 222, students);


		System.out.println(classroom.toString());
	}
}