public class Student {
	private String studentID;
	private String name;
	private String program;
	
	public Student (String name, String studentID, String program) {
		this.studentID = studentID;
		this.name = name;
		this.program = program;
	}
	
	public String getStudentID() {
		return this.studentID;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getProgram() {
		return this.program;
	}
	
	public String toString() {
		return this.name + ": " + this.studentID + ", " + this.program;
	}
}